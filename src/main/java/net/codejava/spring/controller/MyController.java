package net.codejava.spring.controller;

import net.codejava.spring.model.Person;
import net.codejava.spring.service.MyService;

import java.util.Date;
import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyController {
    Date date = new Date();

    @Autowired
    MyService myService;




	@RequestMapping(value = "/process")
	@ResponseBody
	public String startProcessInstance(@RequestParam String assignee) {
		return myService.startProcess(assignee);
	}

	@RequestMapping(value = "/tasks")
	@ResponseBody
	public String getTasks(@RequestParam String assignee) {
		List<Task> tasks = myService.getTasks(assignee);
		return tasks.toString();
	}
	@RequestMapping(value = "/tasks/{assignee}")
	@ResponseBody
	public String getTasks2(@PathVariable("assignee") String assignee) {
		List<Task> tasks = myService.getTasks(assignee);
		return tasks.toString();
	}
	@RequestMapping(value = "/completetask")
	@ResponseBody
	public String completeTask(@RequestParam String id) {
		myService.completeTask(id);
		return "Task with id " + id + " has been completed!";
	}

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    @ResponseBody
    public String home() {
        Person si = new Person();
        si.setName("yacine");
        si.setBirthDate(date);
        myService.saveAndFlush(si);
        return "home :p :p :p :p ";
    }

}
