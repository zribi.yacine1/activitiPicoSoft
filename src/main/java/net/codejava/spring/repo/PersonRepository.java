package net.codejava.spring.repo;

import net.codejava.spring.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {

	Person findByName(String name);

}
