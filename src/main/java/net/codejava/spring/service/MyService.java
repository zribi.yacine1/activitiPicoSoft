package net.codejava.spring.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.codejava.spring.model.Person;
import net.codejava.spring.repo.PersonRepository;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class MyService {
	private static Logger logger = LoggerFactory.getLogger(MyService.class);

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private TaskService taskService;

	@Autowired
	PersonRepository personRepository;


	public String startProcess(String assignee) {
		Person person = personRepository.findByName(assignee);
		System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa " + person.getName());
		logger.warn("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		logger.error("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("person", person);
        String deploymentId = repositoryService.createDeployment().addClasspathResource("processes/simple-process.bpmn20.xml").deploy().getId();
		runtimeService.startProcessInstanceByKey("simpleProcess", variables);

		return processInfo();
	}

	public List<Task> getTasks(String assignee) {
		return taskService.createTaskQuery().taskAssignee(assignee).list();
	}

	public void completeTask(String taskId) {
		taskService.complete(taskId);
	}


	private String processInfo() {
		List<Task> tasks = taskService.createTaskQuery().orderByTaskCreateTime().asc().list();

		logger.warn("tasks  0 " + tasks.get(0));
		logger.warn("tasks  1 " + tasks.get(1));
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("Number of process definitions : "
				+ repositoryService.createProcessDefinitionQuery().count() + "--> Tasks >> ");

		for (Task task : tasks) {
			stringBuilder
					.append(task + " | Assignee: " + task.getAssignee() + " | Description: " + task.getDescription());
		}

		return stringBuilder.toString();
	}

	public Person saveAndFlush(Person si) {
		if (si != null) {
			si = personRepository.saveAndFlush(si);
		}
		return si;
	}
}
